#include <iostream>
#include <fstream>
#include <cmath> 
#include <algorithm>
#include <bitset>
#include <queue>
#include <limits.h>

#include "OTCell.hh"

std::vector<uint8_t> permutations[8];


std::vector<std::vector<std::vector<uint32_t> > > pivot_perms(32, std::vector<std::vector<uint32_t> >(32, std::vector<uint32_t>()));

std::deque<OTCell*> cell_queue;
std::priority_queue<OTCell*, std::vector<OTCell*>, CompareImportance>  importance_queue;

void debug_print_face(MyMesh::FaceHandle face) {
    std::cout << "face " << face << " ";
    MyMesh::FaceHalfedgeIter fh_it;
    for (fh_it=mesh.fh_iter(face); fh_it.is_valid(); ++fh_it) {

        MyMesh::VertexHandle from_vertex = mesh.from_vertex_handle(*fh_it);
        MyMesh::VertexHandle to_vertex = mesh.to_vertex_handle(*fh_it);
        
        std::cout << "(" << from_vertex << ": " << mesh.point(from_vertex) << ") ";
    }
    std::cout << "\n";
}

void debug_patch_print(MyMesh::VertexHandle front_vertex) {
    //Front vertex debug info
    std::cout << "Front vertex: " << front_vertex << " Point:" 
              << mesh.point(front_vertex) << "\n";


    //Faces debug info
    std::cout << "Adjecent faces:\n";
    MyMesh::VertexFaceCCWIter vfccw_it;
    for (vfccw_it=mesh.vf_ccwiter(front_vertex); vfccw_it.is_valid(); ++vfccw_it) {
        debug_print_face(*vfccw_it);
    }

    //Neighbouring vertices debug info
    std::cout << "Neighbouring vertices: \n";
    MyMesh::VertexVertexCCWIter vvccw_it;
    for (vvccw_it=mesh.vv_ccwiter(front_vertex); vvccw_it.is_valid(); ++vvccw_it) {
        std::cout << "Vertex: " << *vvccw_it << "\n";
    }

    //Patch borders debug info
}


void debug_print_bb(int bb[], std::string label) {
    std::cout << "Bounding box " << label << "\n";
    int i;
    for(i = 0; i < 6; i++) {
        std::cout << bb[i] << " ";
    }
    std::cout << "\n";
}

void debug_print_bb(std::vector<int> bb, std::string label) {
    std::cout << "Bounding box " << label << "\n";
    int i;
    for(i = 0; i < 6; i++) {
        std::cout << bb[i] << " ";
    }
    std::cout << "\n";
}


uint32_t rotr(uint32_t v, int32_t shift) {
    int32_t s =  shift>=0? shift%32 : -((-shift)%32);
    return (v>>s) | (v<<(32-s));
}

uint32_t get_permutation(uint32_t current_perm) {
    // current permutation of bits 
    uint32_t v = current_perm;
    // next permutation of bits
    uint32_t w; 

    uint32_t t = v | (v - 1); // t gets v's least significant 0 bits set to 1
    // Next set to 1 the most significant bit to change, 
    // set to 0 the least significant ones, and add the necessary 1 bits.
    w = (t + 1) | (((~t & -~t) - 1) >> (__builtin_ctz(v) + 1));  

    return w;
}

void create_config_table(int t) {
    int i;
    uint8_t first_perm = pow(2, t)-1;
    // uint8_t first_perm = t;

    // std::vector<uint8_t> perms;
    permutations[t].push_back(first_perm); 

    // std::cout << "Perms " << unsigned(first_perm) << " ";
    uint8_t perm = first_perm;
    for(i = 0; i < 100; i++) {    
        perm = get_permutation(perm);
        if(perm <= first_perm) {
            // std::cout << "i: " << i << "\n";
            break;
        }
        permutations[t].push_back(perm);
        // std::cout << unsigned(perm) << " ";
    }   
    // std::cout << "\n";
}

void create_pivot_config_table(int p, int length) {
    std::cout << "\nCreate pivot config table (" << p << "-" << length << "):\n";
    int i;
    uint32_t first_perm = pow(2, p)-1;

    uint32_t perm_cutoff = pow(2, length);

    // std::cout << "Pivot cutoff: " << perm_cutoff << "\n";

    pivot_perms[p][length].push_back(first_perm); 
    // std::cout << "Pivot perms: " << unsigned(first_perm) << " ";
    uint32_t perm = first_perm;
    for(i = 0; i < 1000; i++) {    
        perm = get_permutation(perm);
        if(perm <= first_perm || perm >= perm_cutoff) {
            break;
        }
        pivot_perms[p][length].push_back(perm);
        // std::cout << unsigned(perm) << " ";
    }   
    std::cout << "\n";
}

void get_sorted_weight(float unbalance[], int weights[]) {
    float u[3];
    std::copy(unbalance, unbalance+3, u);
    std::sort(u, u+3);

    weights[0] = 2;
    weights[1] = 2;
    weights[2] = 2;

    if(unbalance[0] <= unbalance[1] && unbalance[0] <= unbalance[2]) {
        weights[0] = 1;
    }
    else if(unbalance[0] > unbalance[1] && unbalance[0] > unbalance[2] &&
            unbalance[1] != unbalance[2]) {
        weights[0] = 3;
    }

    if(unbalance[1] <= unbalance[0] && unbalance[1] <= unbalance[2]) {
        weights[1] = 1;
    }
    else if(unbalance[1] > unbalance[0] && unbalance[1] > unbalance[2] &&
            unbalance[1] != unbalance[2]) {
        weights[1] = 3;
    }

    if(unbalance[2] <= unbalance[0] && unbalance[2] <= unbalance[1]) {
        weights[2] = 1;
    }
    else if(unbalance[2] > unbalance[0] && unbalance[2] > unbalance[1] &&
            unbalance[1] != unbalance[2]) {
        weights[2] = 3;
    }
}

float point_distance(MyMesh::Point a, MyMesh::Point b) {
    float distance = sqrt(pow(b[0]-a[0], 2) + pow(b[1]-a[1], 2) + pow(b[2]-a[2], 2));

    return distance;
}








void OTCell::print_info() {
    std::cout << "Cell info: " 
              << get_id()
              << "\n";
    std::cout << "OTlevel: " << OTlevel
              << ", Vertices: " << vertices.size()
              << ", Neighbors: " << get_valence()
              << ", Centroid: " << centroid
              << "\n";

    std::cout << "Neighbors: ";
    std::set<Cell*>::iterator it;
    for (it = neighbors.begin(); it != neighbors.end(); ++it) {
        std::cout << (*it)->get_id() << " ";
    }
    std::cout << "\n";

    // std::cout << "Vertices: ";
    // int i;
    // for(i = 0; i < vertices.size(); i++) {
    //     std::cout << "(" << mesh.point(vertices[i]) << ") ";
    // }
    // std::cout << "\n";
}

void OTCell::partition() {
    /*
    If OTlevel is max:
        end cell subdivision.

    Else:
        Create 8 empty cells
        For each vertex, assign vertex to cell, and cell to vertex.
        Delete cells without vertices
        For each cell, for each vertex, set cell's neighbor relations.
        NB: neighbor relations go both ways.

        After the children's neighbor relations are set:
        Remove parents neighbor relations.
    */

    int i,j;

    debug_print_bb(&cell_bb[0], "First bb");


    std::cout << "Parent cell size: " << vertices.size() << "\n";

    int x_half = cell_bb[0] + (cell_bb[3] - cell_bb[0])/2;
    int y_half = cell_bb[1] + (cell_bb[4] - cell_bb[1])/2;
    int z_half = cell_bb[2] + (cell_bb[5] - cell_bb[2])/2;

    std::cout << "x_half: " << x_half << "\n";
    std::cout << "y_half: " << y_half << "\n";
    std::cout << "z_half: " << z_half << "\n";

    // Create the bounding boxes for the new cells
    // From and including, to and including.
    int x0y0z0[6] = {cell_bb[0], cell_bb[1], cell_bb[2], 
                     x_half, y_half, z_half};
    int x0y0z1[6] = {cell_bb[0], cell_bb[1], z_half+1, 
                     x_half, y_half, cell_bb[5]};
    int x0y1z0[6] = {cell_bb[0], y_half+1, cell_bb[2], 
                     x_half, cell_bb[4], z_half};
    int x0y1z1[6] = {cell_bb[0], y_half+1, z_half+1, 
                     x_half, cell_bb[4], cell_bb[5]};
    int x1y0z0[6] = {x_half+1, cell_bb[1], cell_bb[2], 
                     cell_bb[3], y_half, z_half};
    int x1y0z1[6] = {x_half+1, cell_bb[1], z_half+1, 
                     cell_bb[3], y_half, cell_bb[5]};
    int x1y1z0[6] = {x_half+1, y_half+1, cell_bb[2], 
                     cell_bb[3], cell_bb[4], z_half};
    int x1y1z1[6] = {x_half+1, y_half+1, z_half+1, 
                     cell_bb[3], cell_bb[4], cell_bb[5]};

    debug_print_bb(x0y0z0, "x0y0z0");
    debug_print_bb(x0y0z1, "x0y0z1");
    debug_print_bb(x0y1z0, "x0y1z0");
    debug_print_bb(x0y1z1, "x0y1z1");
    debug_print_bb(x1y0z0, "x1y0z0");
    debug_print_bb(x1y0z1, "x1y0z1");
    debug_print_bb(x1y1z0, "x1y1z0");    
    debug_print_bb(x1y1z1, "x1y1z1");

    children = new OTCell*[8];
    children[0] = new OTCell(x0y0z0, quantization_bits, 0, OTlevel+1, identifier);
    children[1] = new OTCell(x0y0z1, quantization_bits, 1, OTlevel+1, identifier);
    children[2] = new OTCell(x0y1z0, quantization_bits, 2, OTlevel+1, identifier);
    children[3] = new OTCell(x0y1z1, quantization_bits, 3, OTlevel+1, identifier);
    children[4] = new OTCell(x1y0z0, quantization_bits, 4, OTlevel+1, identifier);
    children[5] = new OTCell(x1y0z1, quantization_bits, 5, OTlevel+1, identifier);
    children[6] = new OTCell(x1y1z0, quantization_bits, 6, OTlevel+1, identifier);
    children[7] = new OTCell(x1y1z1, quantization_bits, 7, OTlevel+1, identifier);
    //Find the bounding box of each cell?
    //For each cell, find cell index? which half of each direction it belongs to.

    std::cout << "Assigning vertices to cells, and vice versa\n";
    //Assign vertex to cell, and cell to vertex
    for(i = 0; i < vertices.size(); i++) {
        MyMesh::Point point = mesh.point(vertices[i]);
        // std::cout << "Point: " << point;

        int cell_index[3] = {0,0,0};
        if(point[0] > x_half) {
            cell_index[0] = 1;
        }
        if(point[1] > y_half) {
            cell_index[1] = 1;
        }
        if(point[2] > z_half) {
            cell_index[2] = 1;
        }

        int child_index = (cell_index[0]*4+cell_index[1]*2+cell_index[2]);

        // std::cout << " Cell index: ";
        // for(j = 0; j < 3; j++) {
        //     std::cout << cell_index[j] << " ";
        // }
        // std::cout << ":" << child_index;
        // std::cout << " \n";

        children[child_index]->add_vertex(vertices[i]);
        mesh.property(v_cell, vertices[i]) = children[child_index];
    }

    //Delete empty cells and set child_count
    for(i = 0; i < 8; i++) {
        if(children[i]->size() == 0) {
            // std::cout << "Deleting empty child-cell " << i << "\n";
            delete children[i];
            children[i] = NULL;
        }
        else {
            child_count += 1;
        }
    }

    //Set cell_configuration
    cell_configuration = 0;
    for(i = 0; i < 8; i++) {
        if(children[i] != NULL) {
            cell_configuration = (1 << i) | cell_configuration;
        }
    }

    std::bitset<8> bits(cell_configuration);
    std::cout << "Cell configuration: " << bits << "\n";
    std::cout << "\n";

    //Set neighbor relations for all vertices contained in this cell
    for(i = 0; i < vertices.size(); i++) {
        //Circulate through adjecent vertices
        MyMesh::VertexVertexIter vv_it;
        for (vv_it=mesh.vv_iter(vertices[i]); vv_it.is_valid(); ++vv_it) {

            // std::cout << "property vertices[i]: " << mesh.property(v_cell,vertices[i])->get_id() << "\n";
            // std::cout << "property neighbor: " << mesh.property(v_cell,*vv_it)->get_id() << "\n";

            //Set neighbor relations if vertices belong to different child cells
            if(mesh.property(v_cell,vertices[i]) != mesh.property(v_cell, *vv_it)) {
                mesh.property(v_cell,vertices[i])->add_neighbor(mesh.property(v_cell, *vv_it));
                mesh.property(v_cell,*vv_it)->add_neighbor(mesh.property(v_cell, vertices[i]));
            }
        }
    }

    std::cout << "OTCell " << identifier << " children info\n";
    for(i = 0; i < 8; i++) {
        if(children[i] != NULL) {
            children[i]->print_info();
        }
    }
    std::cout << "\n";
}


void OTCell::sort_config_table(int t) {
    std::cout << "Sorting config table t" << t << "\n";
    //Sort config table decending according to the cell priority values

    //Create list of permutations if not already there
    // std::cout << permutations.size() << "\n";
    std::cout << "Permutations[t] size: " << permutations[t].size() << "\n";
    if(permutations[t].size() == 0) {
        create_config_table(t);

        std::cout << "Created config table " << t << "\n";
    }

    int i;
    std::cout << "Config table " << t << ":\n";
    for(i = 0; i < permutations[t].size(); i++) {
        std::bitset<8> bits(permutations[t][i]);
        std::cout << bits << " ";
    }
    std::cout << "\n";

    std::sort(permutations[t].begin(), permutations[t].end(), sortFunctor(cell_priority));

    std::cout << "Sorted config table " << t << ":\n";
    for(i = 0; i < permutations[t].size(); i++) {
        std::bitset<8> bits(permutations[t][i]);
        std::cout << bits << " ";
    }
    std::cout << "\n";

}



void OTCell::calculate_cell_priority() {
    int i,j,k;

    //Create bi-partitionings
    //bp[0][0] is 'x1'
    //Bi-partitioning array: first index is the axis (x/y/z),
    // second index is the bi-partitioning subset index
    std::vector<std::vector<std::vector<OTCell*> > > bp(3, std::vector<std::vector<OTCell*> >(2, std::vector<OTCell*>()));

    //Can cast to OTCell because there exists no kd-cells yet

    std::set<Cell*>::iterator it;
    for (it = neighbors.begin(); it != neighbors.end(); ++it) {
        MyMesh::Point c = ((OTCell*) *it)->get_centroid();

        //Add to the bi-partitioning of the x-axis
        if(c[0] <= centroid[0]) {
            bp[0][0].push_back((OTCell*) *it);
        }
        else {
            bp[0][1].push_back((OTCell*) *it);
        }

        //Add to the bi-partitioning of the y-axis
        if(c[1] <= centroid[1]) {
            bp[1][0].push_back((OTCell*) *it);
        }
        else {
            bp[1][1].push_back((OTCell*) *it);
        }

        //Add to the bi-partitioning of the z-axis
        if(c[2] <= centroid[2]) {
            bp[2][0].push_back((OTCell*) *it);
        }
        else {
            bp[2][1].push_back((OTCell*) *it);
        }
    }

    int l[3][2]; //bp distances
    //For each bi-partitioning subset
    for(i = 0; i < 3; i++) {
        for(j = 0; j < 2; j++) {
            //Weighted distance to cells in bipartitioning
            int d = 0;
            for(k = 0; k < bp[i][j].size(); k++) {
                //Distance along the ith axis
                int distance = abs(centroid[i] - bp[i][j][k]->get_centroid()[i]);
                d += bp[i][j][k]->get_OTlevel() * distance;

            }
            l[i][j] = d;

            std::cout << "l["<<i<<"]["<<j<<"] = " << l[i][j] << ", ";
        }
    }
    std::cout << "\n";

    //Get bp weigh of unbalance:
    float unbalance[3] = {0};
    for(i = 0; i < 3; i++) {
        unbalance[i] = abs( float(l[i][0]) / float(l[i][0] + l[i][1]) -0.5);
    }

    int weights[3] = {0};
    get_sorted_weight(unbalance, weights);

    for(i = 0; i < 3; i++) {
        std::cout << "Weight " << i << ": " << weights[i] << ", ";
    }
    std::cout << "\n";

    //Set child-cell priority values
    cell_priority.clear();
    std::cout << "Cell priority values: \n";
    for(i = 0; i < 8; i++) {
        // std::cout << i/4 << " " << (i/2) % 2 << " " << i % 2 << "\n";
        int priority = 0;
        priority += l[0][i/4] * weights[0];
        priority += l[1][(i/2) % 2] * weights[1];
        priority += l[2][i % 2] * weights[2];
        cell_priority.push_back(priority);

        std::cout << i << ": " << cell_priority[i] << ", ";
    }
    std::cout << "\n";
}

void OTCell::set_importance() {
    //Get average distance to neighbors
    float distance_sum = 1;
    std::set<Cell*>::iterator it;
    for (it = neighbors.begin(); it != neighbors.end(); ++it) {
        distance_sum += point_distance(centroid, (*it)->get_centroid());
    }
    float avg_distance = distance_sum/neighbors.size();

    //Get bounding box size 
    int bb_size = abs(cell_bb[3]-cell_bb[0]);

    importance = avg_distance*bb_size*neighbors.size();
}

void OTCell::delete_children() {
    if(children == NULL) {
        return;
    }

    int i;
    for(i = 0; i < 8; i++) {
        if(children[i] != NULL) {
            children[i]->delete_children();
            delete children[i];
        }
    }

    delete[] children;
}



















//Encode functions

void OTCell::create_tree() {
    std::cout << "\n";
    std::cout << identifier << ": Create tree$$$$$$$$$$$$$$$$$$$$$$$$$\n";
    
    std::cout << "OTLevel: " << OTlevel << "\n";
    //DEBUGDEBUG
    // if(OTlevel == 1) {
    //     std::cout << "DEBUG: exiting create_tree prematurely\n";
    //     return;
    // }

    std::cout << "Cell neighbors: " << neighbors.size() << "\n";

    if(OTlevel == quantization_bits) {
        std::cout << "Reached lowest level: " << OTlevel << "\n";
        return;
    }
    int i;

    // Partition cell
    partition();

    // Encode geometry
    encode_geometry();

    // Encode connectivity
    encode_connectivity();


    std::cout << "Removing neighbor relations\n";
    //Remove this cell's neighbor relations. NO?
    std::set<Cell*>::iterator it;
    for (it = neighbors.begin(); it != neighbors.end(); ++it) {
        (*it)->remove_neighbor(this);
    }
    neighbors.clear();

    //Set importance
    for(i = 0; i < 8; i++) {
        if(children[i] != NULL) {
            children[i]->set_importance();
            importance_queue.push(children[i]);
        }
    }

    // std::cout << "Pushing children on the queue\n";
    // for(i = 0; i < 8; i++) {
    //     if(children[i] != NULL) {
    //         cell_queue.push_back(children[i]);
    //     }
    // }

    // Start subdivision of child cells
    // for(i = 0; i < 8; i++) {
    //     if(children[i] != NULL) {
    //         children[i]->create_tree();
    //         // if(i == 0) {
    //             // break;
    //         // }
    //     }
    // }

    // std::cout << "\nFinished recursion, info from cell " << identifier << ":\n";
    // for(i = 0; i < 8; i++) {
    //     if(children[i] != NULL) {
    //         children[i]->print_info();
    //     }
    // }

}

void OTCell::encode_geometry() {
    //Encode the number of nonempty child-cells

    int t = 0;
    int i;

    for(i = 0; i < 8; i++) {
        if(children[i] != NULL) {
            t++;
        }
    }

    output_t(t);

    
    if(t == 8) {
        // No need to encode child-cell configuration tuple
        return;
    }
    
    

    // Calculate priority value for each cell
    calculate_cell_priority();
    std::cout << "Cell priority has been calculated\n";

    // Sort probability table
    sort_config_table(t);


    // Get index in table
    int index = -1;
    for(i = 0; i < permutations[t].size(); i++) {
        if(permutations[t][i] == cell_configuration) {
            index = i;
        }
    }
    if(index == -1) {
        std::cerr << "Could not find cell configuration in table\n";
    }


    // output_index(cell_configuration);
    //Encode index
    output_index(index);


}


void OTCell::output_t(uint8_t t) {
    std::cout << "Encoded T value: " << int(t) << "\n";
    //t--; //Make t fit into 3 bits

    compressed_file.write((char*) &t, 1);

    //Write t to file
}

void OTCell::output_index(uint8_t index) {
    std::cout << "Encoded index value: " << int(index) << "\n";

    compressed_file.write((char*) &index, 1);
    //Write index to file
}


void OTCell::encode_connectivity() {
    int i;
    // Simulate kd-tree subdivision

    //Split, and re-assign this cell's neighbors to kd-children

    std::vector<KdCell*> kdcells;

    KdCell* root_kd = new KdCell(cell_bb, "k0");
    root_kd->add_OTCells(children);
    root_kd->set_neighbors();
    kdcells.push_back(root_kd);

    // root_kd->print_info();

    kdcells[0]->split(0, kdcells);

    //Only split nonempty cells!
    
    for(i = 1; i < 3; i++) {
        if(kdcells[i] != NULL && kdcells[i]->contained_count() != 0) {
            kdcells[i]->split(1, kdcells);
        }
        else {
            kdcells.push_back(NULL);
            kdcells.push_back(NULL);
            std::cout << "Not splitting empty kdcell with index " << i << "\n";

        }
    }

    std::cout << "The two second-level kdcells has been split\n";
    std::cout << "kdcells size: " << kdcells.size() << " (7?)\n";

    for(i = 3; i < 7; i++) {
        if(kdcells[i] != NULL && kdcells[i]->contained_count() != 0) {
            kdcells[i]->split(2, kdcells);
        }
        else {
            kdcells.push_back(NULL);
            kdcells.push_back(NULL);
            std::cout << "Not splitting empty kdcell with index " << i << "\n";
        }
    }

    std::cout << "Splitting resulted " << kdcells.size() << " kdcells (15?)\n";

    //Remove cell->kdcell mappings
    for(i = 0; i < 8; i++) {
        if(children[i] != NULL) {
            children[i]->in_kdcell = NULL;
        }
    }

    //Remove neighbor relations to kdcells
    for(i = 7; i < 15; i++) {
        if(kdcells[i] != NULL) {
            kdcells[i]->remove_neighbors();
        }
    }

    for(i = 0; i < kdcells.size(); i++) {
        delete kdcells[i];
    }

    std::cout << "end of encode_connectivity\n";
}














//Neighborhood funtions

std::vector<MyMesh::VertexHandle> points_nearest_origin(
    std::vector<MyMesh::VertexHandle>& v_neighborhood, int axis) {
    float min = FLT_MAX;

    int i;
    for(i = 0; i < v_neighborhood.size(); i++) {
        float distance = mesh.point(v_neighborhood[i])[axis];
        if(distance < min) {
            min = distance;
        }
    }

    std::vector<MyMesh::VertexHandle> nearest_neighbors;
    for(i = 0; i < v_neighborhood.size(); i++) {
        if(mesh.point(v_neighborhood[i])[axis] == min) {
            nearest_neighbors.push_back(v_neighborhood[i]);
        }
    }

    return nearest_neighbors;
}

std::vector<Cell*> points_nearest_origin(
    std::vector<Cell*>& v_neighborhood, int axis) {
    float min = FLT_MAX;

    int i;
    for(i = 0; i < v_neighborhood.size(); i++) {
        float distance = v_neighborhood[i]->get_centroid()[axis];
        if(distance < min) {
            min = distance;
        }
    }

    std::vector<Cell*> nearest_neighbors;
    for(i = 0; i < v_neighborhood.size(); i++) {
        if(v_neighborhood[i]->get_centroid()[axis] == min) {
            nearest_neighbors.push_back(v_neighborhood[i]);
        }
    }

    return nearest_neighbors;
}

bool cmp_cell_pos(Cell* a, Cell* b) {
    MyMesh::Point point_a = a->get_centroid();
    MyMesh::Point point_b = b->get_centroid();
    if(point_a[0] > point_b[0]) {
        return false;
    }
    else if(point_a[0] == point_b[0] && 
            point_a[1] > point_b[1]) {
        return false;
    }
    else if(point_a[0] == point_b[0] && 
            point_a[1] == point_b[1] &&
            point_a[2] > point_b[2]) {
        return false;
    }

    return true;
}

void rotate_neighborhood(std::vector<Cell*>& v_neighborhood) {
    if(v_neighborhood.size() < 2) {
        return;
    }
    int i;

    // std::cout << "Rotate: neighborhood: \n";
    // for(i = 0; i < v_neighborhood.size(); i++) {
    //     std::cout << "(" << v_neighborhood[i]->get_centroid() << ") ";
    // } 
    // std::cout << "\n";

    std::vector<Cell*> nearest_origin = points_nearest_origin(v_neighborhood, 0);
    // std::cout << "nearest_origin size " << nearest_origin.size() << "\n";

    if(nearest_origin.size() > 1) {
        nearest_origin = points_nearest_origin(nearest_origin, 1);
        // std::cout << "nearest_origin size " << nearest_origin.size() << "\n";
    }
    
    if(nearest_origin.size() > 1) {
        nearest_origin = points_nearest_origin(nearest_origin, 2);
        // std::cout << "nearest_origin size " << nearest_origin.size() << "\n";
    }


    for(i = 0; i < v_neighborhood.size(); i++) {
        if(v_neighborhood[i] == nearest_origin[0]) {
            //Rotate vector so that v_neighborhood is the first element
            std::rotate(v_neighborhood.begin(),
                        v_neighborhood.begin()+i,
                        v_neighborhood.end());
        }
    }
}


void KdCell::get_next_neighbor(std::vector<Cell*>& v_neighborhood,
                       std::set<Cell*>& cells_added, 
                       Cell* c) {
    int i;
    //Get neighbors connected to c and this cell
    std::vector<Cell*> adjecent_n;
    std::set<Cell*>::iterator it;
    for (it = neighbors.begin(); it != neighbors.end(); ++it) {
        if(c->has_neighbor(*it)) {
            adjecent_n.push_back(*it);
        }
    }

    // std::cout << "\nGet next neighbor from " << c->get_id() << "\n";
    // std::cout << "Adjecent neihgbors: ";
    // for(i = 0; i < adjecent_n.size(); i++) {
    //     std::cout << adjecent_n[i]->get_id() << "(" 
    //               << adjecent_n[i]->get_centroid() << ") ";
    // }
    // std::cout << "\n";

    //Sort them by position
    sort(adjecent_n.begin(), adjecent_n.end(), cmp_cell_pos);

    // std::cout << "Sorted adjecent neihgbors: ";
    // for(i = 0; i < adjecent_n.size(); i++) {
    //     std::cout << adjecent_n[i]->get_id() << "(" 
    //               << adjecent_n[i]->get_centroid() << ") ";
    // }
    // std::cout << "\n";

    //For all cells in adjecent_n add to vector and find more neighbors.
    for(i = 0; i < adjecent_n.size(); i++) {
        if(!cells_added.count(adjecent_n[i])) {
            //Only add unvisited neighbors
            v_neighborhood.push_back(adjecent_n[i]);
            cells_added.insert(adjecent_n[i]);
            get_next_neighbor(v_neighborhood, cells_added, adjecent_n[i]);
        }
    }
}

void KdCell::get_neighborhood(std::vector<Cell*>& v_neighborhood) {
    //get ordered neighborhood from neighbors set, and add it to v_neighborhood
    if(neighbors.size() == 0) {
        return;
    }
    int i;

    std::vector<Cell*> neighbor_vector;
    std::copy(neighbors.begin(), neighbors.end(), std::back_inserter(neighbor_vector));

    std::cout << "Neighbor vector: \n";
    for(i = 0; i < neighbor_vector.size(); i++) {
        std::cout << neighbor_vector[i]->get_id() << "(" 
                  << neighbor_vector[i]->get_centroid() << ") ";
    }
    std::cout << "\n";

    //Get the point ordered first by vertex posision, in position [0]
    rotate_neighborhood(neighbor_vector);
    std::cout << "Starting point: " << neighbor_vector[0]->get_id() << "\n";

    std::set<Cell*> cells_added;
    v_neighborhood.push_back(neighbor_vector[0]);
    cells_added.insert(neighbor_vector[0]);
    get_next_neighbor(v_neighborhood, cells_added, neighbor_vector[0]);

    if(v_neighborhood.size() == neighbor_vector.size()) {
        std::cout << "v_neighborhood: \n";
        for(i = 0; i < v_neighborhood.size(); i++) {
            std::cout << v_neighborhood[i]->get_id() << "(" 
                      << v_neighborhood[i]->get_centroid() << ") ";
        }
        std::cout << "\n";
        return;
    }

    while(v_neighborhood.size() < neighbor_vector.size()) {
        std::cout << "Adding disjoint part of neighborhood\n";
        //Add possible disjoint neighborhoods
        //Get non-added neighbors, sort them by position
        std::vector<Cell*> remaining_neighbors;
        for(i = 0; i < neighbor_vector.size(); i++) {
            if(!cells_added.count(neighbor_vector[i])) {
                //neighbor_vector[i] is not in v_neighborhood
                remaining_neighbors.push_back(neighbor_vector[i]);
            }
        }
        //Sort them by position
        sort(remaining_neighbors.begin(), remaining_neighbors.end(), cmp_cell_pos);

        v_neighborhood.push_back(remaining_neighbors[0]);
        cells_added.insert(remaining_neighbors[0]);
        get_next_neighbor(v_neighborhood, cells_added, remaining_neighbors[0]);
    }
    
    std::cout << "v_neighborhood: \n";
    for(i = 0; i < v_neighborhood.size(); i++) {
        std::cout << v_neighborhood[i]->get_id() << "(" 
                  << v_neighborhood[i]->get_centroid() << ") ";
    }
    std::cout << "\n";

    if(v_neighborhood.size() != neighbor_vector.size()) {
        std::cout << "ERROR: COULD NOT SET V_NEIGHBORHOOD CORRECTLY\n";
    }


}














// Decode functions
void OTCell::decode_partition(std::vector<char>& encoded_data, int& file_position) {
    std::cout << "********************Partitioning cell " << identifier 
              << ", level: " << OTlevel << "\n";


    if(OTlevel == quantization_bits) {
        std::cout << "Reached lowest level: " << OTlevel << "\n";
        return;
    }

    int remaining_data = encoded_data.size()-file_position;
    std::cout <<  "Remaining data: " << remaining_data << "\n";
    int i;
    if(remaining_data < 2) {
        std::cout << "No more date to get, cannot decode any more\n";
        //No more data to decode
        return;
    }
   
    decode_geometry(encoded_data, file_position);
    decode_connectivity(encoded_data, file_position);

    std::cout << "OTCell " << identifier << " children info\n";
    for(i = 0; i < 8; i++) {
        if(children[i] != NULL) {
            children[i]->print_info();
        }
    }

    //Set importance, and add children to queue
    for(i = 0; i < 8; i++) {
        if(children[i] != NULL) {
            children[i]->set_importance();
            importance_queue.push(children[i]);
        }
    }

    // Start subdivision of child cells
    // for(i = 0; i < 8; i++) {
    //     if(children[i] != NULL) {
    //         cell_queue.push_back(children[i]);            
    //     }
    // }

    // std::cout << "\nFinished recursion, info from cell " << identifier << ":\n";
    // for(i = 0; i < 8; i++) {
    //     if(children[i] != NULL) {
    //         children[i]->print_info();
    //     }
    // }



}

void OTCell::update_mesh() {
    int i, j, k;
    std::cout << "\nUpdating mesh for cell " << identifier << "\n";

    //Remove current representative vertex and its adjecent faces
    if(rep_vertex.idx() != -1) {
        std::cout << "Removing previous rep_vertex\n";
        std::vector<MyMesh::FaceHandle> faces;

        MyMesh::VertexFaceIter vf_it;
        for (vf_it=mesh.vf_iter(rep_vertex); vf_it.is_valid(); ++vf_it) {
            faces.push_back(*vf_it);
        }

        for(i = 0; i < faces.size(); i++) {
            // std::cout << "deleting face\n";
            mesh.delete_face(faces[i], false);
        }
        mesh.delete_vertex(rep_vertex, false);
    }

    //Create vertices from otcell's children
    MyMesh::VertexHandle vertices[8];
    for(i = 0; i < 8; i++) {
        if(children[i] != NULL) {
            vertices[i] = mesh.add_vertex(children[i]->get_centroid());
            children[i]->set_rep_vertex(vertices[i]);
        }
    }

    std::set<std::string> added_triangles;
    std::cout << "Meshify: \n";
    for(i = 0; i < 8; i++) {
        if(children[i] != NULL) {
            std::set<Cell*> neighbors = children[i]->get_neighbors();
            std::vector<Cell*> n;
            std::copy(neighbors.begin(), neighbors.end(), std::back_inserter(n));

            children[i]->print_info();

            //For all edge pairs:
            for (j = 0; j < n.size(); ++j) {
                for (k = j + 1; k < n.size(); ++k) {
                    /* have unique unordered set {j, k} */
                    // std::cout << n[j]->get_id() << ", " << n[k]->get_id() << "\n";

                    //If both neighbors are connected, create a triangle
                    std::string vertex_ids[3];
                    vertex_ids[0] = children[i]->get_id();
                    vertex_ids[1] = n[j]->get_id();
                    vertex_ids[2] = n[k]->get_id();
                    sort(vertex_ids, vertex_ids+3);


                    std::string triangle_id = vertex_ids[0]+"-"+vertex_ids[1]+"-"+vertex_ids[2];

                    std::cout << "Checking triangle: " << triangle_id << "\n";
                    if(added_triangles.count(triangle_id)) {
                        std::cout << "Triangle already exists: " << triangle_id << "\n";
                    }

                    if(n[j]->has_neighbor(n[k]) and !added_triangles.count(triangle_id)) {
                        std::cout << "Create triangle: " << triangle_id << "\n";

                        // std::cout << "Vertices: " << children[i]->get_rep_vertex()
                        //           << ", " << n[j]->get_rep_vertex()
                        //           << ", " << n[k]->get_rep_vertex() << "\n";


                        std::vector<MyMesh::VertexHandle> face_vhandles;
                        face_vhandles.clear();
                        face_vhandles.push_back(children[i]->get_rep_vertex());
                        face_vhandles.push_back(n[j]->get_rep_vertex());
                        face_vhandles.push_back(n[k]->get_rep_vertex());
                        MyMesh::FaceHandle face = mesh.add_face(face_vhandles);

                        if(face.idx() == -1) {
                            std::cout << "Adding face another way\n";  
                            std::vector<MyMesh::VertexHandle> face_vhandles;
                            face_vhandles.clear();
                            face_vhandles.push_back(children[i]->get_rep_vertex());
                            face_vhandles.push_back(n[k]->get_rep_vertex());
                            face_vhandles.push_back(n[j]->get_rep_vertex());
                            face = mesh.add_face(face_vhandles);
                        }
                        added_triangles.insert(triangle_id);
                    }
                }
            }
        }
    }

    // std::cout << "Writing mesh to file\n";
    // if (!OpenMesh::IO::write_mesh(mesh, "meshify_"+identifier+".off")) 
    // {
    //     std::cerr << "meshify write error\n";
    //     exit(1);
    // }
}

void OTCell::decode_geometry(std::vector<char>& encoded_data, int& file_position) {
    //Create child-cells, according the encoded child-cell configuration

    // Get geometry data
    uint8_t T = (uint8_t) encoded_data[file_position++];
    uint8_t tuple_index = 0;
    uint8_t tuple_value = 0;
    if(T != 8) {
        tuple_index = (uint8_t) encoded_data[file_position++];
        
        //Caculate the priority for each cell
        calculate_cell_priority();
        std::cout << "Cell priority has been calculated\n";
        sort_config_table(T);
        tuple_value = permutations[T][tuple_index];
    }
    else {
        tuple_value = 255;
    }
    std::bitset<8> cell_configuration(tuple_value);
    std::cout << "Cell configuration: " << cell_configuration << "\n";

    child_count = T;

    std::cout << "T: " << int(T) << ", tuple_index: " << int(tuple_index) << "\n";

    int i;

    //DEBUG: cell configuration is stored in file instead of tuple index
    // std::bitset<8> cell_configuration(tuple_index);
    // std::cout << "Cell configuration: " << cell_configuration << "\n";

    //PASTED CODE! OPTIMIZE!
    int x_half = cell_bb[0] + (cell_bb[3] - cell_bb[0])/2;
    int y_half = cell_bb[1] + (cell_bb[4] - cell_bb[1])/2;
    int z_half = cell_bb[2] + (cell_bb[5] - cell_bb[2])/2;

    std::cout << "x_half: " << x_half << "\n";
    std::cout << "y_half: " << y_half << "\n";
    std::cout << "z_half: " << z_half << "\n";

    // Create the bounding boxes for the new cells
    // From and including, to and including.
    int x0y0z0[6] = {cell_bb[0], cell_bb[1], cell_bb[2], 
                     x_half, y_half, z_half};
    int x0y0z1[6] = {cell_bb[0], cell_bb[1], z_half+1, 
                     x_half, y_half, cell_bb[5]};
    int x0y1z0[6] = {cell_bb[0], y_half+1, cell_bb[2], 
                     x_half, cell_bb[4], z_half};
    int x0y1z1[6] = {cell_bb[0], y_half+1, z_half+1, 
                     x_half, cell_bb[4], cell_bb[5]};
    int x1y0z0[6] = {x_half+1, cell_bb[1], cell_bb[2], 
                     cell_bb[3], y_half, z_half};
    int x1y0z1[6] = {x_half+1, cell_bb[1], z_half+1, 
                     cell_bb[3], y_half, cell_bb[5]};
    int x1y1z0[6] = {x_half+1, y_half+1, cell_bb[2], 
                     cell_bb[3], cell_bb[4], z_half};
    int x1y1z1[6] = {x_half+1, y_half+1, z_half+1, 
                     cell_bb[3], cell_bb[4], cell_bb[5]};

    debug_print_bb(x0y0z0, "x0y0z0");
    debug_print_bb(x0y0z1, "x0y0z1");
    debug_print_bb(x0y1z0, "x0y1z0");
    debug_print_bb(x0y1z1, "x0y1z1");
    debug_print_bb(x1y0z0, "x1y0z0");
    debug_print_bb(x1y0z1, "x1y0z1");
    debug_print_bb(x1y1z0, "x1y1z0");    
    debug_print_bb(x1y1z1, "x1y1z1");
    std::cout << "\n";


    children = new OTCell*[8];
    for(i = 0; i < 8; i++) {
        children[i] = NULL;
    }

    if(cell_configuration[0] == 1) {
        children[0] = new OTCell(x0y0z0, quantization_bits, 0, OTlevel+1, identifier); 
    }
    if(cell_configuration[1] == 1) {
        children[1] = new OTCell(x0y0z1, quantization_bits, 1, OTlevel+1, identifier);
    }
    if(cell_configuration[2] == 1) {
        children[2] = new OTCell(x0y1z0, quantization_bits, 2, OTlevel+1, identifier);
    }
    if(cell_configuration[3] == 1) {
        children[3] = new OTCell(x0y1z1, quantization_bits, 3, OTlevel+1, identifier);
    }
    if(cell_configuration[4] == 1) {
        children[4] = new OTCell(x1y0z0, quantization_bits, 4, OTlevel+1, identifier);
    }
    if(cell_configuration[5] == 1) {
        children[5] = new OTCell(x1y0z1, quantization_bits, 5, OTlevel+1, identifier);
    }
    if(cell_configuration[6] == 1) {
        children[6] = new OTCell(x1y1z0, quantization_bits, 6, OTlevel+1, identifier);
    }
    if(cell_configuration[7] == 1) {
        children[7] = new OTCell(x1y1z1, quantization_bits, 7, OTlevel+1, identifier);
    }
}


void OTCell::decode_connectivity(std::vector<char>& encoded_data, int& file_position) {
    /* Simulate kd-tree subdivision
       Update connectivity
    */
    int i;

    std::vector<KdCell*> kdcells;

    KdCell* root_kd = new KdCell(cell_bb, "k0");
    root_kd->add_OTCells(children);
    root_kd->calulate_centroid();
    root_kd->set_rep_vertex(rep_vertex);
    // root_kd->set_neighbors();
    kdcells.push_back(root_kd);

    //Transfer neighbor-relations from this otcell to root_kd
    std::set<Cell*>::iterator it;
    for (it = neighbors.begin(); it != neighbors.end(); ++it) {
        root_kd->add_neighbor(*it);
    }

    root_kd->print_info();

    kdcells[0]->decode_split(0, kdcells, encoded_data, file_position);

    //Only split nonempty cells!
    std::cout << "First kdcell has been split\n";
    
    //Split the two kd-cells into four
    for(i = 1; i < 3; i++) {
        if(kdcells[i] != NULL && kdcells[i]->contained_count() != 0) {
            kdcells[i]->decode_split(1, kdcells, encoded_data, file_position);
        }
        else {
            kdcells.push_back(NULL);
            kdcells.push_back(NULL);
            std::cout << "Not splitting empty kdcell with index: " << i << "\n";
        }
    }
    
    std::cout << "The two second-level kdcells has been split\n";
    std::cout << "kdcells size: " << kdcells.size() << " (7?)\n";

    //Split the four kd-cells into eight
    for(i = 3; i < 7; i++) {
        if(kdcells[i] != NULL && kdcells[i]->contained_count() != 0) {
            kdcells[i]->decode_split(2, kdcells, encoded_data, file_position);
        }
        else {
            kdcells.push_back(NULL);
            kdcells.push_back(NULL);
            std::cout << "Not splitting empty kdcell with index: " << i << "\n";
        }

    }

    std::cout << "Splitting resulted " << kdcells.size() << " kdcells (15?)\n";

    std::cout << "Assigning neighbors from kdcells to otcells\n\n";
    //Assign neighbors from kd-cells to otcells.
    for(i = 0; i < 8; i++) {
        if(children[i] != NULL) 
        {
            // std::cout << "OTcell " << children[i]->get_id() 
            //           << ": " << children[i]->get_centroid()
            //           << " <-> Kdcell " << kdcells[i+7]->get_id()
            //           << ": " << kdcells[i+7]->get_centroid() << "\n";

            std::set<Cell*> kd_neighbors = kdcells[i+7]->get_neighbors();
            std::set<Cell*>::iterator it;
            for(it = kd_neighbors.begin(); it != kd_neighbors.end(); ++it) {
                if((*it)->get_id()[0] == 'k') {
                    children[i]->add_neighbor(((KdCell*) *it)->get_contained_cell(0));
                    ((KdCell*) *it)->get_contained_cell(0)->add_neighbor(children[i]);
                }
                else {
                    children[i]->add_neighbor(*it);
                    (*it)->add_neighbor(children[i]);
                    (*it)->remove_neighbor(kdcells[i+7]);
                }
            } 
        }       
    }

    //Remove this cell's neighbor relations
    std::cout << "Removing neighbor relations for cell " << identifier << "\n";
    for(it = neighbors.begin(); it != neighbors.end(); ++it) {
        (*it)->remove_neighbor(this);
    }
    neighbors.clear();

    std::cout << "Removing cell->kdcell mappings\n";
    //Remove cell->kdcell mappings
    for(i = 0; i < 8; i++) {
        if(children[i] != NULL) {
            children[i]->in_kdcell = NULL;
        }
    }

    for(i = 0; i < kdcells.size(); i++) {
        delete kdcells[i];
    }
}
























//KdCell functions

void KdCell::decode_split(int axis, 
                          std::vector<KdCell*> &kdcells, 
                          std::vector<char>& encoded_data, 
                          int& file_position) {

    std::cout << "\n##################Splitting kdcell " << identifier << "###################\n";
    print_info();

    float half_axis = float(cell_bb[axis]) + (cell_bb[axis+3] - cell_bb[axis])/2.0;
    std::cout << "Half of axis " << axis << ": " << half_axis << "\n";

    std::vector<int> bb_0 = cell_bb;
    bb_0[axis+3] = half_axis;
    KdCell* cell_0 = new KdCell(bb_0, identifier + "1");
    
    std::vector<int> bb_1 = cell_bb;
    bb_1[axis] = half_axis+1;
    KdCell* cell_1 = new KdCell(bb_1, identifier + "2");


    int i, j;
    //Set pointers from otcell to kdcell, and reverse.
    for(i = 0; i < contained_cells.size(); i++) {
        if(contained_cells[i]->get_centroid()[axis] < half_axis) {
            cell_0->add_contained_cell(contained_cells[i]);
            contained_cells[i]->in_kdcell = cell_0;
        }
        else {
            cell_1->add_contained_cell(contained_cells[i]);
            contained_cells[i]->in_kdcell = cell_1;
        }
    }

    //Set centroid based on the contained otcells

    std::cout << "\nKdcell " << cell_0->get_id() << ": \n";
    cell_0->calulate_centroid();
    debug_print_bb(bb_0, "Kdcell");
    cell_0->print_info();

    std::cout << "\nKdcell " << cell_1->get_id() << ": \n";
    cell_1->calulate_centroid();
    debug_print_bb(bb_1, "Kdcell");
    cell_1->print_info();
    std::cout << "\n";

    if(cell_0->contained_count() == 0 || cell_1->contained_count() == 0) {
        //No vertex split will occur if one kdcell is empty
        
        KdCell* nonempty_cell;
        //If one cell is nonempty, assign neighbors to cell and return
        if(cell_0->contained_count() == 0) {
            nonempty_cell = cell_1;
            std::cout << "No vertex split occurring, " 
                      << cell_0->get_id() << " is empty\n";
        }
        else {
            nonempty_cell = cell_0;
            std::cout << "No vertex split occurring, " 
                      << cell_1->get_id() << " is empty\n";
        }


        //Transfer neighbors from this cell to the nonempty cell
        //Remove neighbor relations to this cell.
        std::set<Cell*>::iterator it;
        for (it = neighbors.begin(); it != neighbors.end(); ++it) {
            nonempty_cell->add_neighbor(*it);
            (*it)->add_neighbor(nonempty_cell);            
            (*it)->remove_neighbor(this);
        }

        kdcells.push_back(cell_0);
        kdcells.push_back(cell_1);   

        return;
    }

    //Get information needed to do a vertex split   
    uint8_t P = (uint8_t) encoded_data[file_position++];
    uint32_t pivot_index = *((uint32_t*) &(encoded_data[file_position]));
    file_position += 4;

    uint32_t grouped_segments = *((uint32_t*) &(encoded_data[file_position]));
    file_position += 4;
    uint32_t connected_to = *((uint32_t*) &(encoded_data[file_position]));
    file_position += 4;


    uint8_t adjecency = (uint8_t) encoded_data[file_position++];
    
    
    std::bitset<32> p_bits(pivot_index);
    std::bitset<32> gs_bits(grouped_segments);
    std::bitset<32> ct_bits(connected_to);
    std::cout << "P: " << int(P) 
              << ", pivot_index: " << p_bits
              << ",\ngrouped_segments: " << gs_bits
              << ",\nconnected_to: " << ct_bits
              << ",\n adjacency: " << int(adjecency) << "\n";


    std::vector<Cell*> v_neighborhood;
    get_neighborhood(v_neighborhood);

    //Rotate and possibly reverse neighborhood
    // rotate_neighborhood(v_neighborhood);

    //Remove neighbor relations to this cell.
    std::set<Cell*>::iterator it;
    for (it = neighbors.begin(); it != neighbors.end(); ++it) {
        (*it)->remove_neighbor(this);
    }
    neighbors.clear();

    std::cout << "Neighborhood size: " << v_neighborhood.size() << "\n";


    //Set pivot priority values
    calculate_pivot_priorities(v_neighborhood, 
                               cell_0->get_centroid(), 
                               cell_1->get_centroid());

    sort_pivot_table(P, v_neighborhood.size());

    uint32_t pivot_config = pivot_perms[P][v_neighborhood.size()][pivot_index];




    //Set pivot neighbor relations
    for(i = 0; i < v_neighborhood.size(); i++) {
        std::cout << "i " << i << ": ";
        if(pivot_config & (1 << i)) {
            //v_neighborhood[i] is connected to both
            std::cout << v_neighborhood[i]->get_id() << " is pivot\n";
            v_neighborhood[i]->add_neighbor(cell_0);
            cell_0->add_neighbor(v_neighborhood[i]);
        
            v_neighborhood[i]->add_neighbor(cell_1);
            cell_1->add_neighbor(v_neighborhood[i]);
        }
    }














    //Set non-pivot neighbor relations
    std::cout << "Non-pivot relation assignment:\n";

    //Rotate v_neighborhood so that first vertex is pivot
    for(i = 0; i < v_neighborhood.size(); i++) {
        if(pivot_config & (1 << i)) {
            std::rotate(v_neighborhood.begin(),
                        v_neighborhood.begin()+i,
                        v_neighborhood.end());

            pivot_config = rotr(pivot_config, (int32_t) i);
            break;
        }
    }

    std::bitset<32> pv(pivot_config);
    std::cout << "Rotated pivot_config: " << pv << "\n";

     //Get non-pivot segments
    std::vector<std::vector<Cell*> > segments = create_segments(v_neighborhood,
                                                                pivot_config);

    MyMesh::Point c0 = cell_0->get_centroid();
    MyMesh::Point c1 = cell_1->get_centroid();

    int prediction_index = 0;

    std::cout << "Created segments, assigning cells:\n";
    for(i = 0; i < segments.size(); i++) {

        if(grouped_segments & (1 << i)) {
            MyMesh::Point sc = segment_centroid(segments[i]);

            float d0 = point_distance(sc, c0);
            float d1 = point_distance(sc, c1);

            int prediction = 1;
            if(d0 < d1) {
                prediction = 0;
            }

            bool correct_pred = connected_to & (1 << prediction_index);
            prediction_index++;

            Cell* c;
            if((prediction == 0 && correct_pred) || prediction == 1 && !correct_pred) {
                c = cell_0;
            }
            else {
                c = cell_1;
            }

            for(j = 0; j < segments[i].size(); j++) {
                std::cout << segments[i][j]->get_id() << " is connected to " 
                          << c->get_id() << "\n";
                segments[i][j]->add_neighbor(c);
                c->add_neighbor(segments[i][j]);
            }

        }
        else {
            for(j = 0; j < segments[i].size(); j++) {
                MyMesh::Point sc = segments[i][j]->get_centroid();
                float d0 = point_distance(sc, c0);
                float d1 = point_distance(sc, c1);

                int prediction = 1;
                if(d0 < d1) {
                    prediction = 0;
                }

                bool correct_pred = connected_to & (1 << prediction_index);
                prediction_index++;

                Cell* c;
                if((prediction == 0 && correct_pred) || prediction == 1 && !correct_pred) {
                    c = cell_0;
                }
                else {
                    c = cell_1;
                }

                std::cout << segments[i][j]->get_id() << " is connected to " 
                          << c->get_id() << "\n";
                segments[i][j]->add_neighbor(c);
                c->add_neighbor(segments[i][j]);
            }
        }
    }


    //Set adjacency relation
    if(adjecency) {
        std::cout << cell_0->get_id() << " and " << cell_1->get_id()
                  << " are connected\n";
        cell_0->add_neighbor(cell_1);
        cell_1->add_neighbor(cell_0);
    }

    std::cout << "\n";
    cell_0->print_info();
    cell_1->print_info();

    kdcells.push_back(cell_0);
    kdcells.push_back(cell_1);    
}























void KdCell::split(int axis, std::vector<KdCell*> &kdcells) {
    std::cout << "\nSplitting kdcell " << identifier << "############\n";
    print_info();
    float half_axis = float(cell_bb[axis]) + (cell_bb[axis+3] - cell_bb[axis])/2.0;
    // std::cout << "Half of axis " << axis << ": " << half_axis << "\n";

    std::vector<int> bb_0 = cell_bb;
    bb_0[axis+3] = half_axis;
    KdCell* cell_0 = new KdCell(bb_0, identifier+"1");
    
    std::vector<int> bb_1 = cell_bb;
    bb_1[axis] = half_axis+1;
    KdCell* cell_1 = new KdCell(bb_1, identifier+"2");


    int i, j;
    for(i = 0; i < contained_cells.size(); i++) {
        //Add contained_cells[i]->cell_x relation
        if(contained_cells[i]->get_centroid()[axis] < half_axis) {
            cell_0->add_contained_cell(contained_cells[i]);
            contained_cells[i]->in_kdcell = cell_0;
        }
        else {
            cell_1->add_contained_cell(contained_cells[i]);
            contained_cells[i]->in_kdcell = cell_1;
        }
    }

    cell_0->set_neighbors();
    cell_1->set_neighbors();
    
    cell_0->calulate_centroid();
    cell_1->calulate_centroid();
    
    if(cell_0->contained_count() == 0 || cell_1->contained_count() == 0) {
        //No vertex split will occur
        std::cout << "No vertex split occurring, ";
        if(cell_0->contained_count() == 0) {
            std::cout << cell_0->get_id() << " is empty\n";
        }
        else {
            std::cout << cell_1->get_id() << " is empty\n";
        }

        kdcells.push_back(cell_0);
        kdcells.push_back(cell_1);
    
        
        //Remove neighbor relations to this cell.
        std::set<Cell*>::iterator it;
        for (it = neighbors.begin(); it != neighbors.end(); ++it) {
            (*it)->remove_neighbor(this);

            // Update all related kdcells
            if((*it)->get_id().at(0) == 'k') {
                ((KdCell*) *it)->set_neighbors();
            }
        }

        return;
    }



    std::cout << "\nKdcell " << cell_0->get_id() << ": \n";
    debug_print_bb(bb_0, "Kdcell " + cell_0->get_id());
    cell_0->print_info();

    std::cout << "\nKdcell " << cell_1->get_id() << ": \n";
    debug_print_bb(bb_1, "Kdcell " + cell_1->get_id());
    cell_1->print_info();
    std::cout << "\n";


    //Find connectivity changes
    std::set<Cell*> neighbors_0 = cell_0->get_neighbors();
    std::set<Cell*> neighbors_1 = cell_1->get_neighbors();

    //Get set with the pivot vertices
    std::vector<Cell*> pivot_vertices;
    set_intersection(neighbors_0.begin(),neighbors_0.end(),
                     neighbors_1.begin(),neighbors_1.end(),
                     std::back_inserter(pivot_vertices));
    
    //Output number of pivot vertices
    int P = pivot_vertices.size();
    output_p(P);

    std::vector<Cell*> v_neighborhood;
    get_neighborhood(v_neighborhood);

    if(v_neighborhood.size() > 32) {
        std::cout << "+++++++OVERFLOWERROR++++++++++\n";
    }


    //Set pivot configuration, and pivot segments
    uint32_t pivot_config = 0;
    for(i = 0; i < v_neighborhood.size(); i++) {
        if(neighbors_0.count(v_neighborhood[i]) && 
           neighbors_1.count(v_neighborhood[i])) {
            //If pivot vertex
            pivot_config = (1 << i) | pivot_config;
        }
    }

    std::bitset<32> pbits(pivot_config);
    std::cout << "Pivot config: " << pbits << "\n";




    //Set pivot priority values
    calculate_pivot_priorities(v_neighborhood, 
                               cell_0->get_centroid(), 
                               cell_1->get_centroid());


    sort_pivot_table(P, v_neighborhood.size());



    // Get index in table
    int pivot_index = -1;
    for(i = 0; i < pivot_perms[P][v_neighborhood.size()].size(); i++) {
        if(pivot_perms[P][v_neighborhood.size()][i] == pivot_config) {
            pivot_index = i;
        }
    }
    if(pivot_index == -1) {
        std::cerr << "Could not find cell configuration in table\n";
        std::cout << "Could not find cell configuration in table\n";
    }

    std::cout << "Found config at index: " << pivot_index << "\n";



    output_pivot(pivot_index);






    //Encode non-pivot relations

    //Rotate neighborhood, and segments so that the first item is pivot
    for(i = 0; i < v_neighborhood.size(); i++)  {
        if(neighbors_0.count(v_neighborhood[i]) && 
           neighbors_1.count(v_neighborhood[i])) {
            std::rotate(v_neighborhood.begin(),
                        v_neighborhood.begin()+i,
                        v_neighborhood.end());

            pivot_config = rotr(pivot_config, (int32_t) i);

            break;
        }
    }

    std::bitset<32> pc(pivot_config);
    std::cout << "Rotated pivot_config: " << pc << "\n";

    //Get non-pivot segments
    std::vector<std::vector<Cell*> > segments = create_segments(v_neighborhood,
                                                                pivot_config);

    encode_segments(segments, 
                    neighbors_0, 
                    cell_0->get_centroid(),
                    cell_1->get_centroid());

    //Encode adjecency information
    output_adjecency(neighbors_0.count(cell_1));


    kdcells.push_back(cell_0);
    kdcells.push_back(cell_1);


    std::set<Cell*>::iterator it;
    for (it = neighbors.begin(); it != neighbors.end(); ++it) {
        //Remove neighbor relations to this cell.
        (*it)->remove_neighbor(this);

        // // Update all related kdcells
        if((*it)->get_id().at(0) == 'k') {
            ((KdCell*) *it)->set_neighbors();
        }
    }
}

void KdCell::calculate_pivot_priorities(std::vector<Cell*>& v_neighborhood,
                                        MyMesh::Point c0,
                                        MyMesh::Point c1) {

    int i;
    for(i = 0; i < v_neighborhood.size(); i++) {
        MyMesh::Point nc = v_neighborhood[i]->get_centroid();

        float a = point_distance(nc, c0);
        float b = point_distance(nc, c1);
        float c = point_distance(c0, c1);

        float s = (a + b + c)/2;

        float priority = sqrt(s*(s-a)*(s-b)*(s-c))/(2*s);
        pivot_priorities.push_back(priority);
    }
}

void KdCell::sort_pivot_table(int p, int length) {
    std::cout << "Sorting pivot config table\n";
    //Sort config table decending according to the cell priority values

    //Create list of permutations if not already there
    // std::cout << permutations.size() << "\n";
    std::cout << "pivot_perms[p][length] size: " << pivot_perms[p][length].size() << "\n";
    if(pivot_perms[p][length].size() == 0) {
        create_pivot_config_table(p, length);

        std::cout << "Created pivot config table " << p << "\n";
    }

    int i;
    std::cout << "Pivot config table " << p << ", " << length << ":\n";
    for(i = 0; i < pivot_perms[p][length].size(); i++) {
        std::bitset<32> bits(pivot_perms[p][length][i]);
        std::cout << bits << " ";
    }
    std::cout << "\n";

    std::sort(pivot_perms[p][length].begin(), pivot_perms[p][length].end(), sortFunctor(pivot_priorities));

    std::cout << "Sorted config table " << p << ":\n";
    for(i = 0; i < pivot_perms[p][length].size(); i++) {
        std::bitset<32> bits(pivot_perms[p][length][i]);
        std::cout << bits << " ";
    }
    std::cout << "\n";

}

std::vector<std::vector<Cell*> > KdCell::create_segments(
                            std::vector<Cell*>& v_neighborhood, 
                            uint32_t pivot_config) {

    std::vector<std::vector<Cell*> > segments;

    int i,j;
    int last_piv = -1;
    std::cout << "create_segment: ";
    for(i = 0; i < v_neighborhood.size(); i++)  {
        if(pivot_config & (1 << i)) {
            //Pivot segment, is not encoded.
            std::cout << "-p";
            last_piv = i;
        }
        else {
            if(segments.size() == 0 || i == last_piv+1) {
                segments.push_back(std::vector<Cell*>());
            }
            segments.back().push_back(v_neighborhood[i]);
            std::cout << "-" << v_neighborhood[i]->get_id();
        }
    }
    std::cout << "\n";

    std::cout << "Segment vector: (size: " << segments.size() << ")\n";
    for(i = 0; i < segments.size(); i++) {
        std::cout << i << ": ";
        for(j = 0; j < segments[i].size(); j++) {
            std::cout << segments[i][j]->get_id() << " ";
        }
        std::cout << "\n";
    }

    return segments;
}

void KdCell::encode_segments(std::vector<std::vector<Cell*> >& segments,
                             std::set<Cell*>& neighbors_0,
                             MyMesh::Point c0,
                             MyMesh::Point c1) {
    int i,j;

    uint32_t grouped_segments = 0;
    uint32_t connected_to = 0;

    int encoding_index = 0;

    for(i = 0; i < segments.size(); i++) {
        //Find out if every cell is segment is connected to the same vertex.
        bool first_relation = neighbors_0.count(segments[i][0]);
        bool connected_to_same = true;
        for(j = 1; j < segments[i].size(); j++) {
            if(neighbors_0.count(segments[i][j]) != first_relation) {
                connected_to_same = false;
                break;
            }
        }
        grouped_segments = (connected_to_same << i) | grouped_segments;

        if(connected_to_same && segments[i].size() > 1) {
            // Get vector centroid
            MyMesh::Point sc = segment_centroid(segments[i]);
            std::cout << "Segment vector centroid:";
            std::cout << sc << "\n";
            
            float d0 = point_distance(sc, c0);
            float d1 = point_distance(sc, c1);
            if((d0 < d1 && neighbors_0.count(segments[i][0])) || 
               (d0 >= d1 && !neighbors_0.count(segments[i][0]))) {
                //Prediction is correct
                std::cout << "Correct prediction\n";
                connected_to = (1 << encoding_index) | connected_to;
            }
            else {
                //Prediction is false
                connected_to = (0 << encoding_index) | connected_to;
            }
            encoding_index++;

        }
        else {
            // Get cell centroid
            std::cout << "Segment centroids: \n";
            for(j = 0; j < segments[i].size(); j++) {
                MyMesh::Point sc = segments[i][j]->get_centroid();
                
                float d0 = point_distance(sc, c0);
                float d1 = point_distance(sc, c1);
                if((d0 < d1 && neighbors_0.count(segments[i][j])) || 
                   (d0 >= d1 && !neighbors_0.count(segments[i][j]))) {
                    //Prediction is correct
                    connected_to = (1 << encoding_index) | connected_to;
                }
                else {
                    //Prediction is false
                    connected_to = (0 << encoding_index) | connected_to;
                }
                encoding_index++;

                std::cout << segments[i][j]->get_id() << ": " << sc << "\n";
                std::cout << "Correct prediction\n";
            }
        }
    }

    output_grouped_segments(grouped_segments);
    output_seg_conn(connected_to);    
}



MyMesh::Point KdCell::segment_centroid(std::vector<Cell*>& cells) {
    MyMesh::Point centroid_sum;
    centroid_sum[0] = centroid_sum[1] = centroid_sum[2] = 0.0;
    int i;
    for(i = 0; i < cells.size(); i++) {
        centroid_sum += cells[i]->get_centroid();
    }

    centroid_sum = centroid_sum/cells.size();
    return centroid_sum;
}

void KdCell::output_p(uint8_t p) {
    std::cout << "Encoded p value: " << int(p) << "\n";
    compressed_file.write((char*) &p, 1);
}


void KdCell::output_pivot(uint32_t index) {
    std::bitset<32> bits(index);
    std::cout << "Encoded pivot index: " << bits << "\n";
    compressed_file.write((char*) &index, 4);
}

void KdCell::output_grouped_segments(uint32_t grouped_segments) {
    std::bitset<32> bits(grouped_segments);
    std::cout << "Encoded grouped_segements: " << bits << "\n";
    compressed_file.write((char*) &grouped_segments, 4);
}

void KdCell::output_seg_conn(uint32_t connected_to) {
    std::bitset<32> bits(connected_to);
    std::cout << "Encoded segments connected to: " << bits << "\n";
    compressed_file.write((char*) &connected_to, 4);
}


void KdCell::output_adjecency(bool adjecent) {
    std::cout << "Encoded adjecency value: " << int(adjecent) << "\n";
    compressed_file.write((char*) &adjecent, 1);   
}

void KdCell::add_OTCells(OTCell** &children){
    int i;
    // std::cout << "Cells added to kd: ";
    for(i = 0; i < 8; i++) {
        if(children[i] != NULL) {
            children[i]->in_kdcell = this;
            contained_cells.push_back(children[i]);
            // std::cout << children[i]->get_id() << " ";
        }
    }
    std::cout << "\n";
}

void KdCell::calulate_centroid() {
    int i;
    centroid[0] = 0;
    centroid[1] = 0;
    centroid[2] = 0;
    for(i = 0; i < contained_cells.size(); i++) {
        centroid += contained_cells[i]->get_centroid();
        // std::cout << "Contained centroids: " << contained_cells[i]->get_centroid()
                  // << "\n";
    }
    centroid = centroid/i;
    std::cout << "Kdcell " << identifier << " centroid: " << centroid << "\n";
}

void KdCell::remove_neighbors() {
    std::set<Cell*>::iterator it;
    for (it = neighbors.begin(); it != neighbors.end(); ++it) {
        (*it)->remove_neighbor(this);        
    }
    neighbors.clear();
}

void KdCell::set_neighbors() {
    //Set this cells neighbors based on the neighbors of the contained cells.
    int i;
    for(i = 0; i < contained_cells.size(); i++) {
        std::set<Cell*> contained_neighbors = contained_cells[i]->get_neighbors();

        std::set<Cell*>::iterator it;
        for (it = contained_neighbors.begin(); it != contained_neighbors.end(); ++it) {
            if((*it)->in_kdcell == NULL) {
                neighbors.insert(*it);
                (*it)->add_neighbor(this);
            }
            else {
                neighbors.insert((*it)->in_kdcell);   
                ((*it)->in_kdcell)->add_neighbor(this);
            }
        }
    }
    //Remove neighbor relations to cells contained in this kdcell
    for(i = 0; i < contained_cells.size(); i++) {
        neighbors.erase((Cell*) contained_cells[i]);
        ((Cell*) contained_cells[i])->remove_neighbor((Cell*) contained_cells[i]);
    }
    neighbors.erase(this);
}

void KdCell::print_info() {
    int i;
    std::cout << "KdCell info: " << identifier << "\n";

    std::cout << "Neighbors: ";
    std::set<Cell*>::iterator it;
    for (it = neighbors.begin(); it != neighbors.end(); ++it) {
        std::cout << (*it)->get_id() << " ";
    }
    std::cout << "\n";

    std::cout << "Contained cells: ";
    for(i = 0; i < contained_cells.size(); i++) {
        std::cout << contained_cells[i]->get_id() << ", ";
    }
    std::cout << "\n";
}
