#include <iostream>
#include <stdint.h>
#include <math.h>
// #include <fstream>
#include <queue>
#include <bitset>
#include <algorithm>
// #include <deque>

#include "mesh_utils.hh"
#include "OTCell.hh"

// -------------------- OpenMesh
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

#include <OpenMesh/Core/System/omstream.hh>
// ----------------------------------------------------------------------------

MyMesh mesh;
std::vector<float> mesh_bounding_box(6);
OpenMesh::VPropHandleT<OTCell*> v_cell;
std::ofstream compressed_file;

OTCell* root_cell;

MyMesh building_mesh;
OpenMesh::VPropHandleT<bool> v_visited;
std::set<std::string> added_triangles;

void print_mesh() {
    std::cout << "Printing mesh coordinates";
    MyMesh::VertexIter v_it, v_end(mesh.vertices_end());
    for (v_it=mesh.vertices_begin(); v_it!=v_end; ++v_it) {
        MyMesh::Point v_point = mesh.point(*v_it);
        std::cout << "v_it: " << *v_it << " " << v_point << "\n";
    }
}

void set_tree_vertices(OTCell* otcell) {
    // std::cout << "Setting tree vertices\n";
    OTCell** children = otcell->get_children();
    MyMesh::VertexHandle vertices[8];
    int i;
    for(i = 0; i < 8; i++) {
        if(children[i] != NULL && children[i]->is_leaf()) {
            vertices[i] = building_mesh.add_vertex(children[i]->get_centroid());
            children[i]->set_rep_vertex(vertices[i]);

            building_mesh.property(v_cell, vertices[i]) = children[i];
            building_mesh.property(v_visited, vertices[i]) = false;
        }
        else if(children[i] != NULL) {
            set_tree_vertices(children[i]);

        }
    }
}


MyMesh::HalfedgeHandle get_edge(MyMesh::VertexHandle v1, MyMesh::VertexHandle v2) {
    MyMesh::HalfedgeHandle he;

    MyMesh::VertexOHalfedgeIter voh_it;
    for (voh_it=building_mesh.voh_iter(v1); voh_it.is_valid(); ++voh_it) {

        if(building_mesh.to_vertex_handle(*voh_it) == v2) {
            he = *voh_it;
            break;
        }
    }


    return he;
}


void create_initial_face(MyMesh::VertexHandle vertex) {
    std::cout << "Creating initial face\n";
    OTCell* otcell = building_mesh.property(v_cell, vertex);
    std::set<Cell*> neighbors = otcell->get_neighbors();
    std::vector<Cell*> n;
    std::copy(neighbors.begin(), neighbors.end(), std::back_inserter(n));

    int i,j,k;
    //For all edge pairs:
    for (j = 0; j < n.size(); ++j) {
        for (k = j + 1; k < n.size(); ++k) {
            //If both neighbors are connected, create a triangle
            if(n[j]->has_neighbor(n[k])) {
                std::cout << "Vertices: " 
                          << otcell->get_rep_vertex() << "(" 
                          << otcell->get_id() <<")" 
                          << ", " << n[k]->get_rep_vertex() << "(" 
                          << building_mesh.property(v_cell, n[k]->get_rep_vertex())->get_id() <<")" 
                          << ", " << n[j]->get_rep_vertex() << "(" 
                          << building_mesh.property(v_cell, n[j]->get_rep_vertex())->get_id() <<")" 
                          << "\n";

                std::vector<MyMesh::VertexHandle> face_vhandles;
                face_vhandles.clear();
                face_vhandles.push_back(otcell->get_rep_vertex());
                face_vhandles.push_back(n[j]->get_rep_vertex());
                face_vhandles.push_back(n[k]->get_rep_vertex());
                MyMesh::FaceHandle face = building_mesh.add_face(face_vhandles);

                if(face.idx() == -1) {
                    std::cout << "~~~ERROR: UNABLE TO ADD FACE (inital face)~~~\n";
                }

                return;
            }

        }
    }
}

void meshify_process_patch(MyMesh::VertexHandle vertex, 
                           std::deque<MyMesh::VertexHandle>& vertex_queue) {
    //Add faces adjecent to vertex
    std::cout << "~~~~~Meshify vertex " << vertex << " (" 
              << building_mesh.point(vertex) << ")~~~~~~~~~\n";
    int i,j,k;

    OTCell* otcell = building_mesh.property(v_cell, vertex);

    std::set<Cell*> neighbors = otcell->get_neighbors();
    std::vector<Cell*> n;
    std::copy(neighbors.begin(), neighbors.end(), std::back_inserter(n));

    std::set<MyMesh::VertexHandle> neighbor_vertices;

    //Add non-visited neighboring vertices at the end of the queue.
    for(i = 0; i < n.size(); i++) {
        neighbor_vertices.insert(n[i]->get_rep_vertex());
        if(building_mesh.property(v_visited, n[i]->get_rep_vertex()) == false) {
            // vertex_queue.push_back(n[i]->get_rep_vertex());
        }
    }


    //DEBUG
    std::cout << "Number of neighbors: " << neighbor_vertices.size() << "\n";;
    std::set<MyMesh::VertexHandle>::iterator it;
    for (it = neighbor_vertices.begin(); it != neighbor_vertices.end(); ++it) {
        std::cout << *it << "("
         << building_mesh.point(*it) << ") ";
    }
    std::cout << "\n";

    //DEBUG END

    MyMesh::FaceHandle first_face;

    //Set the first face. Either an existing face in the patch, or a newly created face
    //bordering a face in another patch.
    if(building_mesh.valence(vertex) != 0) {
        std::cout << "found exisitng patch face\n";
        //Find an adjecent face.
        first_face = *building_mesh.vf_ccwiter(vertex);
    }
    else {
        std::cout << "exisitng patch face could not be found\n";

        //Find and edge shared by two neighbors, and create the first face adjecent to it.
        std::set<MyMesh::VertexHandle>::iterator it;
        for (it = neighbor_vertices.begin(); it != neighbor_vertices.end(); ++it) {
            MyMesh::VertexOHalfedgeIter voh_it;
            for (voh_it=building_mesh.voh_iter(*it); voh_it.is_valid(); ++voh_it) {
                if(neighbor_vertices.count(building_mesh.to_vertex_handle(*voh_it))) {
                    //If vertices/cells are neighbors, create face.

                    std::cout << "Vertices: " << vertex
                          << ", " << building_mesh.from_vertex_handle(*voh_it)
                          << ", " << building_mesh.to_vertex_handle(*voh_it) << "\n";    


                    // std::cout << "create initial patch face\n";
                    std::vector<MyMesh::VertexHandle> face_vhandles;
                    face_vhandles.clear();
                    face_vhandles.push_back(vertex);
                    face_vhandles.push_back(building_mesh.from_vertex_handle(*voh_it));
                    face_vhandles.push_back(building_mesh.to_vertex_handle(*voh_it));
                    MyMesh::FaceHandle face = building_mesh.add_face(face_vhandles);

                    if(face.idx() == -1) {
                        // std::cout << "Adding face another way\n";  
                        std::vector<MyMesh::VertexHandle> face_vhandles;
                        face_vhandles.clear();
                        face_vhandles.push_back(vertex);
                        face_vhandles.push_back(building_mesh.to_vertex_handle(*voh_it));
                        face_vhandles.push_back(building_mesh.from_vertex_handle(*voh_it));
                        face = building_mesh.add_face(face_vhandles);
                    }

                    if(face.idx() == -1) {
                        std::cout << "~~~ERROR: UNABLE TO ADD FACE (first patch face)~~~\n";
                    }
                    
                    first_face = face;

                    it = neighbor_vertices.end();
                    it--;
                    break;
                }
            }
        }
    }

    //DEBUG
    if(first_face.idx() == -1) {
        return;
    }

    // std::cout << "beginning face creation\n";
    //Start with a triangle. 
    //create the remaning faces.
    //When we go around to the first face again, end.
    MyMesh::VertexHandle current_vertex;
    MyMesh::VertexHandle previous_vertex;

    //Initialize current_vertex and previous_vertex as the vertices defining the face's
    //edge opposite to the center vertex.
    MyMesh::FaceVertexIter fv_it;
    for (fv_it=building_mesh.fv_iter(first_face); fv_it.is_valid(); ++fv_it) {
        if(*fv_it != vertex && current_vertex.idx() == -1) {
            current_vertex = *fv_it;
        }
        else if(*fv_it != vertex && current_vertex.idx() != -1) {
            previous_vertex = *fv_it;
        }
    }


    int adjecent_faces = 1;
    // while(adjecent_faces < neighbor_vertices.size()) {
    for(i = 1; i < neighbor_vertices.size(); i++) {
        std::cout << "current_vertex: " << current_vertex 
                  << ", previous_vertex: " << previous_vertex << "\n";
        // std::cout << "Adjecent face: " << adjecent_faces << "\n";
        MyMesh::HalfedgeHandle current_edge = get_edge(vertex, current_vertex);
        //Found existing adjecent face
        //If center -> current edge has triangles on both sides, go to next.
        if(current_edge.idx() != -1 && 
           !building_mesh.is_boundary(current_edge) &&
           !building_mesh.is_boundary(building_mesh.opposite_halfedge_handle(current_edge))) {

            std::set<MyMesh::VertexHandle>::iterator it;
            //Find next neighbor to current vertex which is not previous_vertex
            for (it = neighbor_vertices.begin(); it != neighbor_vertices.end(); ++it) {  
                //Do not go back to the previous vertex
                if(*it == previous_vertex) {
                    continue;
                }

                //Get the next vertex in the circulation around the center vertex
                MyMesh::HalfedgeHandle he = get_edge(current_vertex, *it);
                if(he.idx() != -1) {
                    previous_vertex = current_vertex;
                    current_vertex = *it;
                    break;
                }
            }
            adjecent_faces++;
            continue;
        }
        
        //Create new adjecent face
        std::set<MyMesh::VertexHandle>::iterator it;
        for (it = neighbor_vertices.begin(); it != neighbor_vertices.end(); ++it) {  
            //Do not go back to the previouis vertex
            if(*it == previous_vertex) {
                continue;
            }

            //Get the next vertex in the circulation around the center vertex
            if(building_mesh.property(v_cell, current_vertex)
                ->has_neighbor(building_mesh.property(v_cell, *it))) {
                //If neighbor to current_vertex 

                std::cout << "Vertices: " << vertex
                          << ", " << current_vertex
                          << ", " << *it << "\n";     

                //Create face
                std::vector<MyMesh::VertexHandle> face_vhandles;
                face_vhandles.clear();
                face_vhandles.push_back(vertex);
                face_vhandles.push_back(current_vertex);
                face_vhandles.push_back(*it);
                MyMesh::FaceHandle face = building_mesh.add_face(face_vhandles);

                if(face.idx() == -1) {
                    // std::cout << "Adding face another way\n";  
                    std::vector<MyMesh::VertexHandle> face_vhandles;
                    face_vhandles.clear();
                    face_vhandles.push_back(vertex);
                    face_vhandles.push_back(*it);
                    face_vhandles.push_back(current_vertex);
                    face = building_mesh.add_face(face_vhandles);
                }

                if(face.idx() == -1) {
                    std::cout << "~~~ERROR: UNABLE TO ADD FACE (additional face)~~~\n";
                }

                previous_vertex = current_vertex;
                current_vertex = *it;
                adjecent_faces++;
                break;
            }
        }
    }


    //Add all adjacent vertices to queue.
    MyMesh::VertexVertexIter vv_it;
    for (vv_it=building_mesh.vv_iter(vertex); vv_it.is_valid(); ++vv_it) {
        vertex_queue.push_back(*vv_it);
    }
}

void process_meshify_queue(std::deque<MyMesh::VertexHandle>& vertex_queue) {
    int i = 0;
    while(!vertex_queue.empty()) {
        MyMesh::VertexHandle v = vertex_queue.front();
        vertex_queue.pop_front();
        if(building_mesh.property(v_visited, v) == false) {
            building_mesh.property(v_visited, v) = true;
            meshify_process_patch(v, vertex_queue);
        } 
        i++;
    }
    std::cerr << "Meshified " << i << " patches\n";
}

void create_mesh_iterative() {
    std::cout << "Create mesh iteratively\n";
    building_mesh.add_property(v_cell);
    
    set_tree_vertices(root_cell);


    MyMesh::VertexHandle first_vertex = *building_mesh.vertices_begin();
    create_initial_face(first_vertex);

    std::deque<MyMesh::VertexHandle> vertex_queue;
    vertex_queue.push_back(first_vertex);

    process_meshify_queue(vertex_queue);


    //Iterate through all vertices, find possible disjoint pieces.
    MyMesh::VertexIter v_it, v_end(building_mesh.vertices_end());
    for (v_it=building_mesh.vertices_begin(); v_it!=v_end; ++v_it) {
        if(building_mesh.property(v_visited, *v_it) == false) {
            std::cerr << "Processing disjoint peice\n\n";
            vertex_queue.push_back(*v_it);
            create_initial_face(*v_it);
            process_meshify_queue(vertex_queue);
        }
    }

    if (!OpenMesh::IO::write_mesh(building_mesh, "meshify_iter.off")) 
    {
        std::cerr << "meshify write error\n";
        exit(1);
    }
}

// void geometry_from_tree(OTCell* cell) {
//     int i;
//     if(cell->is_leaf()) {
//         MyMesh::Point centroid = cell->get_centroid();
//         building_mesh.add_vertex(centroid);
//         std::cout << "Vertex position: " << centroid << "\n";
//     }
//     else {
//         OTCell** children = cell->get_children();
//         for(i = 0; i < 8; i++) {
//             if(children[i] != NULL) {
//                 geometry_from_tree(children[i]);
//             }
//         }
//     }
// }

// void connectivity_from_tree(OTCell* cell) {
//     int i;
//     if(cell->is_leaf()) {
//         std::set<Cell*> neighbors = cell->get_neighbors();
//         std::set<Cell*>::iterator it;
//         for (it = neighbors.begin(); it != neighbors.end(); ++it) {
//             MyMesh::Point centroid = (*it)->get_centroid();

//             MyMesh::VertexIter v_it, v_end(building_mesh.vertices_end());
//             for (v_it=building_mesh.vertices_begin(); v_it!=v_end; ++v_it) {

//             }
//         }
//     }
//     else {
//         OTCell** children = cell->get_children();
//         for(i = 0; i < 8; i++) {
//             if(children[i] != NULL) {
//                 connectivity_from_tree(children[i]);
//             }
//         }
//     }
// }

// void mesh_from_tree() {
//     geometry_from_tree(root_cell);
//     connectivity_from_tree(root_cell);

//     if (!OpenMesh::IO::write_mesh(building_mesh, "mesh_from_tree.off")) 
//     {
//         std::cerr << "write error\n";
//         exit(1);
//     }
// }

void initialize_tree(int quant_bits) {
    int quant_res = (int) pow(2, quant_bits);

    //Get bounding box
    mesh_bounding_box = get_bounding_box(mesh);
    //Quantize mesh coordinates
    mesh = quantize_coordinates(mesh, mesh_bounding_box, quant_bits);

    //Get bounding box from quantized mesh
    int quantized_bb[6] = {0, 0 ,0, quant_res-1, quant_res-1, quant_res-1};

    // Create initial cell
    root_cell = new OTCell(quantized_bb, quant_bits, 0, 0, "");

    // Add every vertex to initial octree cell
    std::vector<MyMesh::VertexHandle> vertices;
    MyMesh::VertexIter v_it, v_end(mesh.vertices_end());
    for (v_it=mesh.vertices_begin(); v_it!=v_end; ++v_it) {
        vertices.push_back(*v_it);
    }
    root_cell->set_vertices(vertices);
    root_cell->set_importance();
}

void cleanup() {
    compressed_file.close();

    root_cell->delete_children();
    delete root_cell;
}


void print_tree(OTCell* cell) {
    int i;
    OTCell** children = cell->get_children();
    for(i = 0; i < 8; i++) {
        if(children[i] != NULL) {
            children[i]->print_info();
            // print_tree(children[i]);
        }
    }
}

int main(int argc, char* argv[]) {
    /*
    Usage: otcoder input_mesh output_mesh
    */

    // Read input mesh
    if (!OpenMesh::IO::read_mesh(mesh, argv[1])) 
    {
        std::cerr << "read error\n";
        exit(1);
    }

    compressed_file.open("encoded.txt");

    mesh.add_property(v_cell);

    initialize_tree(10);

    int i = 0;
    importance_queue.push(root_cell);
    while(!importance_queue.empty()) {
        OTCell* cell = importance_queue.top();
        importance_queue.pop();
        cell->create_tree();
        
        std::cout << "create_tree operation: " << i << "\n";
        if(i == 300) { //Cow max = 11858,  880v = 300.
            // break;
        }
        i++;
    }

    // print_tree(root_cell);

    building_mesh.add_property(v_visited);

    compressed_file.close();
    create_mesh_iterative();

    // if (!OpenMesh::IO::write_mesh(mesh, "output.off")) 
    // {
    //     std::cerr << "write error\n";
    //     exit(1);
    // }

    cleanup();

    return 0;

}