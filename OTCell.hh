
#ifndef OTCELL_HH
#define OTCELL_HH

#include <iostream>
#include <stdint.h>
#include <fstream>
#include <string>
#include <deque>

#include "mesh_utils.hh"

class KdCell;

class Cell {
private:

public:
    virtual void print_info() = 0;

    virtual std::set<Cell*> get_neighbors() = 0;
    virtual void add_neighbor(Cell* cell) = 0;
    virtual void remove_neighbor(Cell* cell) = 0;
    virtual bool has_neighbor(Cell* cell) = 0;

    virtual std::string get_id() = 0;

    virtual MyMesh::Point get_centroid() = 0;
    virtual MyMesh::VertexHandle get_rep_vertex() = 0;
    
    KdCell* in_kdcell = NULL;
};

class OTCell : public Cell{
private:
    //All the vertices contained within this cell
    std::vector<MyMesh::VertexHandle> vertices;

    //The bounding box of this cell
    std::vector<int> cell_bb;

    //Identifies cell's position in octree
    //012 identifies the cell as the second child of the first cell created from the root.
    std::string identifier; 
    
    //Coordianats of this cell's centroid
    MyMesh::Point centroid;

    //Pointers to child-cells, if the cell has been subdivided.
    OTCell** children = NULL; 
    int child_count;
    uint8_t cell_configuration;

    //3-bit position in parent-cells configuration
    int position; 

    //Octree level, level 0 is root.
    int OTlevel; 

    int quantization_bits;

    int importance;

    //The cells connected to this cell by neighboring vertices
    std::set<Cell*> neighbors;

    std::vector<int> cell_priority; //Priorities of child cells

    //The cell's representative vertex
    MyMesh::VertexHandle rep_vertex;

    //http://stackoverflow.com/questions/8372918/c-std-list-sort-with-custom-comparator-that-depends-on-an-member-variable-for
    struct sortFunctor {
        const std::vector<int>& cell_priority;
        sortFunctor(const std::vector<int>& cell_priority) : cell_priority(cell_priority) { }
        bool operator()(int a, int b) { 
            int i;

            //Get tuple priority for a and b
            int tuple_pri_a = 0;
            int tuple_pri_b = 0;
            for(i = 0; i < 8; i++) {
                tuple_pri_a += ((a >> 7-i) & 1) * cell_priority[i]; 
                tuple_pri_b += ((b >> 7-i) & 1) * cell_priority[i]; 
            }    

            return tuple_pri_a > tuple_pri_b;
        }
    };

public:
    OTCell(int bb[], int quantization_bits, int position, int OTlevel, std::string parent_id):
        quantization_bits(quantization_bits),
        position(position),
        OTlevel(OTlevel) {
            std::vector<int> v(bb, bb + 6*sizeof(int));
            this->cell_bb = v;

            identifier = parent_id + (char) (position+48);

            //Calculate centroid
            float x_half = (cell_bb[3] - cell_bb[0])/2.0;
            float y_half = (cell_bb[4] - cell_bb[1])/2.0;
            float z_half = (cell_bb[5] - cell_bb[2])/2.0;
            centroid[0] = (float(cell_bb[0]) + x_half);
            centroid[1] = (float(cell_bb[1]) + y_half);
            centroid[2] = (float(cell_bb[2]) + z_half); 

            child_count = 0;  
    }

    void set_vertices(std::vector<MyMesh::VertexHandle> vertices) {
        this->vertices = vertices;
    }

    void add_vertex(MyMesh::VertexHandle v) {
        vertices.push_back(v);
    }
    std::set<Cell*> get_neighbors() {
        return neighbors;
    }
    void add_neighbor(Cell* cell) {
        neighbors.insert(cell);
    }
    void remove_neighbor(Cell* cell) {
        neighbors.erase(cell);
    }
    bool has_neighbor(Cell* cell) {
        return neighbors.count(cell);
    }
    int get_valence() {
        return neighbors.size();
    }
    std::string get_id() {
        return identifier;
    }
    MyMesh::Point get_centroid() {
        return centroid;
    }
    void set_rep_vertex(MyMesh::VertexHandle rep_vertex) {
        //Set the cell's representative vertex
        this->rep_vertex = rep_vertex;
    }
    MyMesh::VertexHandle get_rep_vertex() {
        return rep_vertex;
    }
    int get_OTlevel() {
        return OTlevel;
    }
    int size() {
        return vertices.size();
    }
    bool is_leaf() {
        return child_count == 0;
    }
    int get_importance() {
        return importance;
    }

    OTCell** get_children() {
        return children;
    }

    void sort_config_table(int t);

    void calculate_cell_priority();

    void print_info();

    void create_tree();
    void partition();
    void encode_geometry();
    void encode_connectivity();

    void set_importance();

    void update_mesh();
    void decode_partition(std::vector<char>& encoded_data, int& file_position);
    void decode_geometry(std::vector<char>& encoded_data, int& file_position);
    void decode_connectivity(std::vector<char>& encoded_data, int& file_position);

    void output_t(uint8_t t);
    void output_index(uint8_t index);

    void delete_children();
};

class KdCell : public Cell {
private:
    std::string identifier; 
    MyMesh::Point centroid;
    std::set<Cell*> neighbors;

    MyMesh::VertexHandle rep_vertex;

    std::vector<OTCell*> contained_cells;

    //The bounding box of this cell
    std::vector<int> cell_bb;

    std::vector<float> pivot_priorities;

    struct sortFunctor {
        const std::vector<float>& pivot_priorities;
        sortFunctor(const std::vector<float>& pivot_priorities) : pivot_priorities(pivot_priorities) { }
        bool operator()(uint32_t a, uint32_t b) { 
            int i;

            //Get tuple priority for a and b
            float tuple_pri_a = 0;
            float tuple_pri_b = 0;
            for(i = 0; i < pivot_priorities.size(); i++) {
                tuple_pri_a += ((a >> (pivot_priorities.size()-1)-i) & 1) * pivot_priorities[i]; 
                tuple_pri_b += ((b >> (pivot_priorities.size()-1)-i) & 1) * pivot_priorities[i]; 
            }    

            return tuple_pri_a > tuple_pri_b;
        }
    };
public:
    KdCell(std::vector<int> cell_bb, std::string identifier):
        identifier(identifier),
        cell_bb(cell_bb) {
    }
    std::set<Cell*> get_neighbors() {
        return neighbors;
    }
    void add_neighbor(Cell* cell) {
        neighbors.insert(cell);
    }
    void remove_neighbor(Cell* cell) {
        neighbors.erase(cell);
    }
    bool has_neighbor(Cell* cell) {
        return neighbors.count(cell);
    }
    void add_contained_cell(OTCell* cell) {
        contained_cells.push_back(cell);
    }
    OTCell* get_contained_cell(int i) {
        return contained_cells[i];
    }
    int contained_count() {
        return contained_cells.size();
    }
    std::string get_id() {
        return identifier;
    }
    MyMesh::Point get_centroid() {
        return centroid;
    }
    void set_rep_vertex(MyMesh::VertexHandle rep_vertex) {
        //Set the cell's representative vertex
        this->rep_vertex = rep_vertex;
    }
    MyMesh::VertexHandle get_rep_vertex() {
        return rep_vertex;
    }

    void print_info();

    void add_OTCells(OTCell** &children);
    void set_neighbors();
    void remove_neighbors();
    void get_neighborhood(std::vector<Cell*>& v_neighborhood);
    void get_next_neighbor(std::vector<Cell*>& v_neighborhood,
                       std::set<Cell*>& cells_added, 
                       Cell* neighbor);

    void calulate_centroid();

    void split(int axis, std::vector<KdCell*> &kdcells);

    void decode_split(int axis, 
                      std::vector<KdCell*> &kdcells, 
                      std::vector<char>& encoded_data, 
                      int& file_position);


    void calculate_pivot_priorities(std::vector<Cell*>& v_neighborhood,
                                    MyMesh::Point c0,
                                    MyMesh::Point c1);
    void sort_pivot_table(int p, int length);

    std::vector<std::vector<Cell*> > create_segments(
                            std::vector<Cell*>& v_neighborhood, 
                            uint32_t pivot_config);

    void encode_segments(std::vector<std::vector<Cell*> >& segments,
                             std::set<Cell*>& neighbors_0,
                             MyMesh::Point c0,
                             MyMesh::Point c1);

    MyMesh::Point segment_centroid(std::vector<Cell*>& cells);

    void output_p(uint8_t p);
    void output_pivot(uint32_t index);
    void output_nonpivot(uint32_t index);
    void output_adjecency(bool adjecent);

    void output_grouped_segments(uint32_t grouped_segments);
    void output_seg_conn(uint32_t connected_to);
};


class CompareImportance
{
public:
     bool operator() (OTCell* a, OTCell* b) {
        return a->get_importance() < b->get_importance();
    }
};

extern OpenMesh::VPropHandleT<OTCell*> v_cell;

extern std::ofstream compressed_file;

extern std::deque<OTCell*> cell_queue;

extern std::priority_queue<OTCell*, std::vector<OTCell*>, CompareImportance>  importance_queue;

#endif