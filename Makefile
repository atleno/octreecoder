CC = g++
INCLUDE = -I/home/atle/Documents/Master/Code/OpenMesh-4.1/src/
DIR = -L/home/atle/Documents/Master/Code/OpenMesh-4.1/build/Build/lib/
LIB = -lOpenMeshCore -lOpenMeshTools


all: mesh_utils OTCell otcoder otdecoder


otdecoder: otdecoder.cc mesh_utils.hh mesh_utils.o OTCell.o
	$(CC) -o otdecoder otdecoder.cc mesh_utils.o OTCell.o $(INCLUDE) $(DIR) $(LIB)

otcoder: otcoder.cc mesh_utils.hh mesh_utils.o OTCell.o
	$(CC) -o otcoder otcoder.cc mesh_utils.o OTCell.o $(INCLUDE) $(DIR) $(LIB)

mesh_utils: mesh_utils.cc mesh_utils.hh
	$(CC) -c mesh_utils.cc $(INCLUDE) $(DIR) $(LIB)

OTCell: OTCell.cc OTCell.hh
	$(CC) -c OTCell.cc $(INCLUDE) $(DIR) $(LIB)



encode: 
	./otcoder quantized_cow.off

decode:
	./otdecoder encoded.txt

valgrind_encode:
	valgrind --track-origins=yes ./otcoder quantized_cow.off

valgrind_decode:
	valgrind --track-origins=yes ./otdecoder encoded.txt

clean:
	rm -f otcoder mesh_utils.o OTCell.o