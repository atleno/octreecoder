#include <iostream>
#include <stdint.h>
#include <math.h>
#include <fstream>
#include <queue>
#include <bitset>

#include "mesh_utils.hh"

std::vector<float> get_bounding_box(MyMesh mesh) {
    /*
    Return vector with bounding box coordinates
    Values are ordered like this: [x_min, y_min, z_min, x_max, y_max, z_max]
    */
    std::vector<float> bounding_box(6);
    bounding_box[0] = FLT_MAX;
    bounding_box[1] = FLT_MAX;
    bounding_box[2] = FLT_MAX;
    bounding_box[3] = FLT_MIN;
    bounding_box[4] = FLT_MIN;
    bounding_box[5] = FLT_MIN;

    MyMesh::VertexIter v_it, v_end(mesh.vertices_end());
    for (v_it=mesh.vertices_begin(); v_it!=v_end; ++v_it) {
        MyMesh::Point v_point = mesh.point(*v_it);

        if(v_point[0] < bounding_box[0]) {
            bounding_box[0] = v_point[0];
        }
        if(v_point[1] < bounding_box[1]) {
            bounding_box[1] = v_point[1];
        }
        if(v_point[2] < bounding_box[2]) {
            bounding_box[2] = v_point[2];
        }
        
        if(v_point[0] > bounding_box[3]) {
            bounding_box[3] = v_point[0];
        }
        if(v_point[1] > bounding_box[4]) {
            bounding_box[4] = v_point[1];
        }
        if(v_point[2] > bounding_box[5]) {
            bounding_box[5] = v_point[2];
        }
    }

    return bounding_box;
}

MyMesh quantize_coordinates(MyMesh mesh, std::vector<float> bounding_box, int bits) {
    /*
    Quantize vertex coordinates to specified number of bits, 
    and shifts the mesh into the positive range.
    */
    int quantization_resolution = (int) pow(2, bits);
    int i;
    
    std::cout << "Quantization resolution: " << quantization_resolution << "\n";
    
    //Get ranges
    float ranges[3];
    ranges[0] = bounding_box[3]-bounding_box[0];
    ranges[1] = bounding_box[4]-bounding_box[1];
    ranges[2] = bounding_box[5]-bounding_box[2];

    float max_range = 0.0f;

    for(i = 0; i < 3; i++) {
        if(ranges[i] > max_range) {
            max_range = ranges[i];
        }
    }

    std::cout << "Max range: " << max_range << "\n";

    //Quantize coordinates
    MyMesh::VertexIter v_it, v_end(mesh.vertices_end());
    for (v_it=mesh.vertices_begin(); v_it!=v_end; ++v_it) {
        MyMesh::Point v_point = mesh.point(*v_it);

        // std::cout << "Old point: " << v_point << "  ";
        for(i = 0; i < 3; i++) {
            //For each coordinate, shift and quantize value
            
            //Shift value
            if(bounding_box[i] < 0.0f) {
                v_point[i] = v_point[i] + -1*bounding_box[i];
            }
            else {
                v_point[i] = v_point[i] - bounding_box[i]; 
            }

            //Quantize value
            v_point[i] = floorf((v_point[i]/max_range)*(quantization_resolution-1));
        }
        // std::cout << "new point: " << v_point << "\n";

        mesh.set_point(*v_it, v_point);
    }

    return mesh;
}