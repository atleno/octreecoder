#ifndef MESHUTILS
#define MESHUTILS

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

typedef OpenMesh::TriMesh_ArrayKernelT<> MyMesh;
extern MyMesh mesh;
extern MyMesh building_mesh;

std::vector<float> get_bounding_box(MyMesh mesh);
MyMesh quantize_coordinates(MyMesh mesh, std::vector<float> bounding_box, int bits);

#endif